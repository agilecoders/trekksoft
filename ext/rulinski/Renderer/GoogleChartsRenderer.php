<?php

namespace rulinski\Renderer;

class GoogleChartsRenderer
{
    private $google_api_link = 'https://chart.googleapis.com/chart?cht=qr';
    private static $instance = false;
    public $text = 'Hello World';
    public $width = 50;
    public $height = 50;
    
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    public function __construct()
    {
        return self::getInstance();
    }
    
    public function setParams($text = false, $width = false, $height = false)
    {
        $this->text   = $text ?? $text;
        $this->width  = $width ?? $width;
        $this->height = $height ?? $height;
    }
    
    public function buildQuery()
    {
        return $this->google_api_link . "&chl={$this->text}&choe=UTF-8&chs={$this->width}x{$this->height}";
    }
    
    public function generate()
    {
        header('Content-type:image/png');
        readfile(self::buildQuery());
    }
}