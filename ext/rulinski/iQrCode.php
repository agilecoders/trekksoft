<?php

namespace rulinski\QrCode;

interface iQrCode
{
    /**
     * @param $object
     *
     * @return mixed
     */
    public function setRenderer($object);
    
    /**
     * @return image data
     */
    public function generate();
    
}