<?php

namespace rulinski\QrCode;

class QrCode implements iQrCode
{
    public $text = 'Hello World';
    public $width = 50;
    public $height = 50;
    protected $renderer = false;
    
    public function __construct($text = false, $width = false, $height = false)
    {
        $this->text   = $text ?? $text;
        $this->width  = $width ?? $width;
        $this->height = $height ?? $height;
    }
    
    public function setRenderer($object)
    {
        $this->renderer = $object;
    }
    
    public function generate()
    {
        $this->renderer->setParams($this->text, $this->width, $this->height);
        
        return $this->renderer->generate();
    }
}