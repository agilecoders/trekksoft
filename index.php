<?php

require_once __DIR__ . '/vendor/autoload.php';

use rulinski\QrCode\QrCode;
use rulinski\Renderer\GoogleChartsRenderer;

try {
    $qrCode = new QrCode('TrekkSoft', 100, 100); // text, width, height
    $qrCode->setRenderer(new GoogleChartsRenderer());
    $qrCodeData = $qrCode->generate(); // should return the image data, not the URL
} catch (Exception $e) {
    //var_dump($e->getMessage());
}




